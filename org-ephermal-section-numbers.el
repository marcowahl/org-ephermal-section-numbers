;;; org-ephermal-section-numbers.el --- Display natural section numbering -*- lexical-binding: t; -*-


;;; Header:

;; Copyright 2016 Marco Wahl

;; Author: Marco Wahl <marcowahlsoft@gmail.com>
;; Maintainer: Marco Wahl <marcowahlsoft@gmail.com>
;; Created: [2016-11-02 Wed 11:27]
;; Version: See git tag.
;; Keywords: reading, convenience, chill
;; URL: https://github.com/marcowahl/rope-read-mode

;; This file is not part of Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Place overlays with the natural section numbering in the
;; headings.

;; This program is non intrusive it just can add the numbers to an
;; org-mode buffer.

;; There is just command `org-ephermal-section-numbers-toggle' to turn
;; the numbering on or off.


;;; Code:

(let (ols it-s-on
          (org-experimental-ephermal-section-numbers-auto-off nil ;; "3 sec"
                                                              ))

  (defun org-ephermal-section-numbers-toggle ()
    "Toggle ephermal-section-numbers."
    (interactive)
    (assert (eq major-mode 'org-mode))
    (if it-s-on
        (org-ephermal-section-numbers-unravel)
      (org-ephermal-section-numbers-establish))
    (setq it-s-on (not it-s-on)))

  (defun org-ephermal-section-numbers-establish ()
    "Place overlays with the numbering over the headline stars."
    (setq numbering '((0 . 0)))
    (org-map-entries
     (lambda ()
       (cond
        ((< (car (car numbering)) (org-outline-level))
         (setq numbering  (cons (cons (org-outline-level) 1) numbering)))
        ((= (car (car numbering)) (org-outline-level))
         (setf (cdr (car numbering)) (incf (cdr (car numbering)))))
        (t
         (while (> (car (car numbering)) (org-outline-level))
           (setq numbering (cdr numbering)))
         (setf (cdr (car numbering)) (incf (cdr (car numbering))))))
       (push (make-overlay (point) (1- (+ (point) (org-outline-level)))) ols)
       (overlay-put (car ols) 'invisible t)
       (push (make-overlay (1- (+ (point) (org-outline-level))) (+ (point) (org-outline-level))) ols)
       (overlay-put (car ols)
                    'display (concat "." (mapconcat
                                          (lambda (x) (number-to-string (cdr x)))
                                          (cdr (reverse  numbering)) "."))))
     t nil)
    (when org-experimental-ephermal-section-numbers-auto-off ; nil or e.g. "3 sec"
      (run-at-time org-experimental-ephermal-section-numbers-auto-off
                   nil #'org-ephermal-section-numbers-toggle)))

  (defun org-ephermal-section-numbers-unravel ()
    "Remove all overlays pertaining the numbering."
    (dolist (ol ols)
      (delete-overlay ol))
    (setf ols nil)))


(provide 'org-ephermal-section-numbers)


;;; org-ephermal-section-numbers.el ends here
