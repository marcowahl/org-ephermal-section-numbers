# What and How #

Display the natural section numbering for an Org mode buffer.

There is just command

    org-ephermal-section-numbers-toggle

to turn the numbering on or off.

The numbering is just decoration and non-intrusive.
